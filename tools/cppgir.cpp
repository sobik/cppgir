#include "CLI/App.hpp"
#include "common.hpp"
#include "fs.hpp"
#include "genbase.hpp"
#include "genns.hpp"
#include "repository.hpp"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <CLI/CLI.hpp>

#include <charconv>
#include <map>
#include <set>
#include <vector>

// thanks go to glib
#define GI_STRINGIFY(macro_or_string) GI_STRINGIFY_ARG(macro_or_string)
#define GI_STRINGIFY_ARG(contents) #contents

Log _loglevel = Log::WARNING;

class Generator
{
  GeneratorContext &ctx_;
  std::vector<fs::path> girdirs_;
  // processes ns (ns: ns header)
  std::map<std::string, std::string> processed_;

public:
  Generator(GeneratorContext &_ctx, const std::vector<fs::path> &girdirs)
      : ctx_(_ctx), girdirs_(girdirs)
  {}

  std::string find_in_dir(const fs::path p, const std::string &ns) const
  {
    std::string result;
    fs::error_code ec;
    // check if in this directory
    if (!fs::is_directory(p, ec))
      return "";

    auto f = p / (ns + GIR_SUFFIX);
    std::vector<fs::directory_entry> dirs;
    for (auto &&entry : fs::directory_iterator(p, ec)) {
      if (fs::is_directory(entry, ec)) {
        dirs.emplace_back(entry);
      } else if (f == entry) {
        // exact match
        result = f.native();
      } else {
        // non-version match
        auto ename = entry.path().filename().native();
        auto s = ns.size();
        if (ename.substr(0, s) == ns && ename.size() > s && ename[s] == '-')
          result = entry.path().native();
      }
    }
    // check dirs
    while (!dirs.empty() && result.empty()) {
      result = find_in_dir(dirs.back(), ns);
      dirs.pop_back();
    }
    return result;
  }

  std::string find(const std::string &ns) const
  {
    for (auto &&d : girdirs_) {
      auto res = find_in_dir(fs::path(d), ns);
      if (!res.empty())
        return res;
    }
    return "";
  }

  // gir might be:
  // + a GIR filename
  // + or a GIR namespace (ns-version)
  // + or a GIR namespace (no version appended)
  void generate(const std::string &gir, bool recurse)
  {
    fs::path f(gir);
    fs::error_code ec;
    auto path = fs::exists(f, ec) ? gir : find(gir);
    if (path.empty())
      throw std::runtime_error("could not find GIR for " + gir);
    // avoid reading if possible
    if (processed_.count(gir))
      return;
    auto genp = NamespaceGenerator::new_(ctx_, path);
    auto &gen = *genp;
    // normalize namespace
    auto &&ns = gen.get_ns();
    // prevent duplicate processing
    if (processed_.count(ns))
      return;
    // generate deps
    auto &&deps = gen.get_dependencies();
    std::vector<std::string> headers;
    if (recurse) {
      for (auto &&d : deps) {
        generate(d, recurse);
        // should be available now
        headers.emplace_back(processed_[d]);
      }
    }
    // now generate this one
    // also mark processed and retain ns header file
    processed_[ns] = gen.process_tree(headers);
  }
};

namespace
{

int
die(const CLI::App &desc, const std::string &msg = "")
{
  std::cout << msg << std::endl << std::endl;
  ;
  std::cout << desc.help() << std::endl;
  return 1;
}

template<typename T, std::enable_if_t<std::disjunction_v<std::is_integral<T>,
                         std::is_floating_point<T>>>>
T
convert(std::string_view value)
{
  T result;
  std::from_chars(std::begin(value), std::end(value), result);
  return result;
}

template<typename T, std::enable_if_t<std::is_same_v<T, fs::path>>>
fs::path
convert(std::string_view value)
{
  return value;
}

template<typename T>
T
convert(T value)
{
  return value;
}

template<typename T>
std::optional<T>
get_env(const char *name)
{
  auto value = std::getenv(name);
  if (value != nullptr) {
    return convert<T>(std::string_view(value));
  }
  return std::nullopt;
}

template<typename T>
std::optional<T>
get_env(const char *name, T def)
{
  return get_env<T>(name).value_or(def);
}

template<typename T>
void
addsplit(std::vector<T> &target, const std::string &src,
    const std::string &seps = ":")
{
  std::vector<std::string> tmp;
  boost::split(tmp, src, boost::is_any_of(seps));
  for (auto &&d : tmp) {
    if (d.size())
      target.emplace_back(d);
  }
}

template<typename T>
std::vector<T>
get_env_split(const char *name, const std::string &seps = ":")
{
  auto v = std::getenv(name);
  if (v != nullptr) {
    std::vector<T> result;
    addsplit(result, v, seps);
    return result;
  }
  return {};
}

} // namespace

int
main(int argc, char *argv[])
{
  (void)argc;
  (void)argv;

  GeneratorOptions options;

  std::vector<std::string> girs_env = get_env_split<std::string>("GI_GIR");
  std::vector<std::string> girs;
  std::vector<fs::path> ignore_files_env = get_env_split<fs::path>("GI_IGNORE");
  std::vector<fs::path> ignore_files;
  std::vector<fs::path> suppress_files_env =
      get_env_split<fs::path>("GI_SUPPRESSION");
  std::vector<fs::path> suppress_files;
  auto fpath_gen_suppress = get_env<fs::path>("GI_GEN_SUPPRESSION");
  std::vector<fs::path> gir_path_env = get_env_split<fs::path>("GI_GIR_PATH");
  std::vector<fs::path> gir_path;

  // then collect CLI settings
  CLI::App app{"Supported options"};
  app.add_option("--debug", _loglevel, "debug level")->check(CLI::Range(1, static_cast<int>(logger_level_names.size())));
  app.add_option("--gir", girs, "debug level");
  app.add_option("gir", girs, "GIR to process");
  app.add_option("--ignore", ignore_files, "ignore files")->delimiter(':');
  app.add_option("--suppression", suppress_files, "suppression files")
      ->delimiter(':');
  app.add_option(
      "--gen-suppression", fpath_gen_suppress, "generate suppression file");
  app.add_option("--output", options.rootdir, "output directory")
      ->envname("GI_OUTPUT")
      ->required();
  app.add_option("--gir-path", gir_path, "GIR search path")->delimiter(':');
  app.add_flag("--class", options.classimpl, "generate class implementation")
      ->envname("GI_CLASS");
  app.add_flag(
         "--class-full", options.classfull, "generate fallback class methods")
      ->envname("GI_CLASS_FULL");
  app.add_flag(
         "--dl", options.dl, "use dynamic dlopen/dlsym rather than static link")
      ->envname("GI_DL");
  app.add_flag("--expected", options.expected,
         "use expected<> return rather than exception")
      ->envname("GI_EXPECTED");
  app.add_flag("--optional", options.optional,
         "use std::optional<> for nullable (string) result")
      ->envname("GI_OPTIONAL");

  CLI11_PARSE(app, argc, argv);

  // merge environment and CLI settings
  girs.insert(girs.end(), std::make_move_iterator(girs_env.begin()),
      std::make_move_iterator(girs_env.end()));
  gir_path.insert(gir_path.end(), std::make_move_iterator(gir_path_env.begin()),
      std::make_move_iterator(gir_path_env.end()));
  ignore_files.insert(ignore_files.end(),
      std::make_move_iterator(ignore_files_env.begin()),
      std::make_move_iterator(ignore_files_env.end()));
  suppress_files.insert(suppress_files.end(),
      std::make_move_iterator(suppress_files_env.begin()),
      std::make_move_iterator(suppress_files_env.end()));

  // system default
  {
    // gobject-introspection uses XDG_DATA_DIRS
    auto default_gir_dirs = get_env_split<fs::path>("XDG_DATA_DIRS");
#ifdef DEFAULT_GIRPATH
    // optional fallback
    if (default_gir_dirs.empty())
      addsplit(default_gir_dirs, GI_STRINGIFY(DEFAULT_GIRPATH));
#endif
    for (auto &d : default_gir_dirs)
      gir_path.push_back(d / "gir-1.0");
  }

  for (auto &&d : gir_path)
    logger(Log::DEBUG, "extending GIR path {}", d);

    // system default
#ifdef DEFAULT_IGNORE_FILE
  addsplit(ignore_files, GI_STRINGIFY(DEFAULT_IGNORE_FILE));
#endif

  // sanity check
  if (girs.empty())
    return die(app, "nothing to process");

  // check for now
  if (gir_path.empty())
    return die(app, "empty search path");

  // at least the standard ignore file is required
  // or things will go wrong
  int cnt = 0;
  fs::error_code ec;
  for (auto &f : ignore_files)
    cnt += fs::exists(f, ec);

  if (cnt == 0)
    return die(app, "required default ignore file location not specified");

  auto match_ignore = Matcher(ignore_files);
  auto match_suppress = Matcher(suppress_files);

  // now let's start
  logger(Log::INFO, "generating to directory {}", options.rootdir);

  auto repo = Repository::new_();
  std::set<std::string> suppressions;
  GeneratorContext ctx{
      options, *repo, match_ignore, match_suppress, suppressions};
  Generator gen(ctx, gir_path);
  try {
    for (auto &&g : girs)
      gen.generate(g, true);

    // write suppression
    if (fpath_gen_suppress.has_value()) {
      std::vector<std::string> sup(suppressions.begin(), suppressions.end());
      sort(sup.begin(), sup.end());
      logger(Log::INFO, "writing {} suppressions to {}", sup.size(),
          fpath_gen_suppress.value());

      std::ofstream fsup(fpath_gen_suppress.value());
      for (auto &&v : sup)
        fsup << v << std::endl;
    }
  } catch (std::runtime_error &ex) {
    logger(Log::ERROR, ex.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
